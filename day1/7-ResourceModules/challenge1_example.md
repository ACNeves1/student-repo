---
- name: configure VLANs
  hosts: arista, juniper
  gather_facts: false

  tasks:

  - name: use vlans resource module
    arista.eos.vlans:
      state: merged
      config:
        - name: desktops
          vlan_id: 20
        - name: servers
          vlan_id: 30
        - name: printers
          vlan_id: 40
        - name: DMZ
          vlan_id: 50
    when: ansible_network_os == 'eos'

  - name: use vlans resource module
    junipernetworks.junos.junos_vlans:
      state: merged
      config:
        - name: desktops
          vlan_id: 20
        - name: servers
          vlan_id: 30
        - name: printers
          vlan_id: 40
        - name: DMZ
          vlan_id: 50
    when: ansible_network_os == 'junos'

